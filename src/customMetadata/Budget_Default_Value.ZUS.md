<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ZUS</label>
    <protected>false</protected>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">12.0</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Expense Category</value>
    </values>
    <values>
        <field>Value__c</field>
        <value xsi:type="xsd:double">1172.56</value>
    </values>
</CustomMetadata>

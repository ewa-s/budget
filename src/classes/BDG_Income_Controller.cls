public with sharing class BDG_Income_Controller {

	public String selectedBudgetId { get; set; }
	public List<IncomeItem> incomeItemsForBudget { get; set; }

	public Integer rowToDelete { get; set; }
	private List<BDG_Income__c> incomeItemsToDelete { get; set; }

	public BDG_Income_Controller() {
		selectedBudgetId = getCurrentBudgetId();
		incomeItemsForBudget = retrieveIncomeItemsForBudget();
		incomeItemsToDelete = new List<BDG_Income__c>();
	}

	public List<SelectOption> getBudgets() {
		List<SelectOption> result = new List<SelectOption>();

		for (BDG_Monthly_Budget__c budget : [SELECT Id, Name FROM BDG_Monthly_Budget__c ORDER BY Name]) {
			result.add(new SelectOption(budget.Id, budget.Name));
		}

		return result;
	}

	private String getCurrentBudgetId() {
		String result;
		Date now = Date.today();
		BDG_Monthly_Budget__c currentBudget = [SELECT Id FROM BDG_Monthly_Budget__c 
												WHERE CALENDAR_YEAR(Date__c) = :now.year()
													AND CALENDAR_MONTH(Date__c) = :now.month()];
		if (currentBudget != NULL) {
			result = currentBudget.Id;
		}
		return result;
	}

	private List<IncomeItem> retrieveIncomeItemsForBudget() {
		List<IncomeItem> result = new List<IncomeItem>();
		Integer i = 0;
		for (BDG_Income__c income : [SELECT Id, Name, Amount__c
										FROM BDG_Income__c
										WHERE Month_Budget__c = :selectedBudgetId
										ORDER BY CreatedDate ASC]) {
			result.add(new IncomeItem(i, income));
			i++;
		}
		return result;
	}

	public String getMenuConfig() {
		return BDG_Menu_Controller.getItemsForPage('/budget/apex/BDG_Income');
	}

	public PageReference changeBudget() {
		incomeItemsForBudget = retrieveIncomeItemsForBudget();
		return null;
	}

	public PageReference save() {
		List<BDG_Income__c> incomeToUpsert = new List<BDG_Income__c>();
		for (IncomeItem item : incomeItemsForBudget) {
			item.income.Month_Budget__c = selectedBudgetId;
			incomeToUpsert.add(item.income);
		}
		upsert incomeToUpsert;
		delete incomeItemsToDelete;
		return null;
	}

	public PageReference add() {
		incomeItemsForBudget.add(new IncomeItem(incomeItemsForBudget.size(), new BDG_Income__c()));
		return null;
	}

	public PageReference remove() {
		incomeItemsToDelete.add(incomeItemsForBudget[rowToDelete].income);

		incomeItemsForBudget.remove(rowToDelete);
		for (Integer i = 0; i < incomeItemsForBudget.size(); i++) {
			incomeItemsForBudget[i].rowNr = i;
		}
		return null;
	}

	public class IncomeItem {
		public Integer rowNr { get; set; }
		public BDG_Income__c income { get; set; }

		public IncomeItem(Integer rowNr, BDG_Income__c income) {
			this.rowNr = rowNr;
			this.income = income;
		}

	}
}
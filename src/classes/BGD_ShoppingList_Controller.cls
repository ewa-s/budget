global with sharing class BGD_ShoppingList_Controller {
	public BGD_ShoppingList_Controller() {
		
	}

	public String getExistingList() {
		return JSON.serialize([SELECT Id, Name, Item_Name__c, Order__c, Status__c 
								FROM List_Item__c 
								ORDER BY Order__c, CreatedDate]);
	}

	@RemoteAction
	global static List_Item__c saveListItem(String listItemJson) {
		List_Item__c listItem = (List_Item__c)JSON.deserialize(listItemJson, List_Item__c.class);
		upsert listItem;
		return listItem;
	}

	@RemoteAction
	global static void deleteListItem(String recordId) {
		Database.delete(recordId);
	}

	@RemoteAction
	global static Boolean itemsChanged(String itemsIdsJson) {
		Set<List_Item__c> currentItems = (Set<List_Item__c>)JSON.deserialize(itemsIdsJson, Set<List_Item__c>.class);
		Set<List_Item__c> queriedItems = new Set<List_Item__c>([SELECT Id, Item_Name__c FROM List_Item__c]);
		return !currentItems.equals(queriedItems);
	}

    public String getMenuConfig() {
        return BDG_Menu_Controller.getItemsForPage('/budget/apex/BDG_ShoppingList');
    }
}
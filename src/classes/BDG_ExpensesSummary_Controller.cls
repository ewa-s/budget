public with sharing class BDG_ExpensesSummary_Controller {
	
	public String selectedBudgetId { get; set; }
	public Decimal monthProgress;

	public BDG_ExpensesSummary_Controller() {
		getCurrentBudgetInfo();
	}

	public List<SelectOption> getBudgets() {
		List<SelectOption> result = new List<SelectOption>();

		for (BDG_Monthly_Budget__c budget : [SELECT Id, Name FROM BDG_Monthly_Budget__c ORDER BY Name]) {
			result.add(new SelectOption(budget.Id, budget.Name));
		}

		return result;
	}

	private void getCurrentBudgetInfo() {
		String result;
		Date now = Date.today();
		BDG_Monthly_Budget__c currentBudget = [SELECT Id, Month_progress__c FROM BDG_Monthly_Budget__c 
												WHERE CALENDAR_YEAR(Date__c) = :now.year()
													AND CALENDAR_MONTH(Date__c) = :now.month()];
		if (currentBudget != NULL) {
			selectedBudgetId = currentBudget.Id;
			monthProgress = currentBudget.Month_progress__c;
		}
	}

	public List<BDG_Category_Balance__c> getCategoriesForMonth() {
		return [SELECT Id, Category__c, Budgeted_Amount__c, Spent__c, Available_to_Spend__c, Percentage_Spent__c 
				FROM BDG_Category_Balance__c 
				WHERE Month_Budget__c = :selectedBudgetId
				ORDER BY Name];
	}

	public Decimal getMonthProgress() {
		Date now = Date.today();
		BDG_Monthly_Budget__c currentBudget = [SELECT Id, Month_progress__c FROM BDG_Monthly_Budget__c 
												WHERE Id = :selectedBudgetId];

		if (currentBudget != null) {
			return currentBudget.Month_progress__c;
		}
		else {
			return 0;
		}
	}
	
    public String getMenuConfig() {
        return BDG_Menu_Controller.getItemsForPage('/budget/apex/BDG_ExpensesSummary');
    }
}
public with sharing class BDG_Expense_Ext {

	private BDG_Expense__c mysObject;
    private ApexPages.StandardController stdController;
    public Date selectedDate { get; set; }
    public String budgetedAmounts { get; set; }
    public Decimal amount { get; set; }

    public BDG_Expense_Ext(ApexPages.StandardController stdController) {
        this.stdController = stdController;
        this.mysObject = (BDG_Expense__c)stdController.getRecord();

        setTodayForDate();
    }

    private void setTodayForDate() {
        mysObject.Date__c = Date.today();
        selectedDate = mysObject.Date__c;
    }

    public List<SelectOption> getAvailableCategoriesWithAmounts() {        
        mysObject.Date__c = selectedDate;
        List<SelectOption> availableCategories = new List<SelectOption>();

        Integer selectedYear = mysObject.Date__c.year();
        Integer selectedMonth = mysObject.Date__c.month();

        List<BDG_Category_Balance__c> categories = [
                SELECT Id, Category__c, Budgeted_Amount__c, Spent__c 
                FROM BDG_Category_Balance__c
                WHERE CALENDAR_YEAR(Month_Budget__r.Date__c) = :selectedYear
                    AND CALENDAR_MONTH(Month_Budget__r.Date__c) = :selectedMonth
                ORDER BY Order__c ASC
            ];

        Map<String, BudgetAmount> budgetedAmountsMap = new Map<String, BudgetAmount>();

        for (BDG_Category_Balance__c category : categories) {
            availableCategories.add(new SelectOption(category.Id, category.Category__c));
            budgetedAmountsMap.put(category.Id, new BudgetAmount(category.Budgeted_Amount__c, category.Budgeted_Amount__c - category.Spent__c));
        }

        budgetedAmounts = JSON.serialize(budgetedAmountsMap);

        return availableCategories;
    }

    
    public String getMenuConfig() {
        return BDG_Menu_Controller.getItemsForPage('/budget/apex/BDG_AddExpense');
    }


    public PageReference save() {
        mysObject.Date__c = selectedDate;
        mysObject.Amount__c = amount;
        try {
            insert mysObject;
        }
        catch (Exception ex) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage());
            ApexPages.addMessage(myMsg);
            return null;
        }

        PageReference pr = Page.BDG_AddExpense;
        pr.setRedirect(true);
        return pr;
    }

    public class BudgetAmount {
        public Decimal budgeted;
        public Decimal available;

        public BudgetAmount(Decimal budgeted, Decimal available) {
            this.budgeted = budgeted;
            this.available = available;
        }
    }
}
public with sharing class BDG_Menu_Controller {
	public String menuItemsString;
	public List<MenuItem> items {get; set;}

	public BDG_Menu_Controller() {	
	}

	private void parseMenuItemsString() {
		items = (List<MenuItem>)JSON.deserialize(menuItemsString, List<MenuItem>.class);
	}

	public String getMenuItemsString() {
		return menuItemsString;
	}

	public void setMenuItemsString(String input) {
		menuItemsString = input.replaceAll('\'', '"');
		parseMenuItemsString();
	}

	public class MenuItem {
		public String name { get; set; }
		public String href { get; set; }
		public Boolean active { get; set; }

		public MenuItem(String name, String href, Boolean active) {
			this.name = name;
			this.href = href;
			this.active = active;
		}
	}

	public static String getItemsForPage(String pageUrl) {
		List<MenuItem> config = new List<MenuItem> {
			new MenuItem('Add Expense', '/budget/apex/BDG_AddExpense', pageUrl == '/budget/apex/BDG_AddExpense'),
			new MenuItem('Expenses List', '/budget/apex/BDG_ExpensesReport', pageUrl == '/budget/apex/BDG_ExpensesReport'),
			new MenuItem('Expenses Summary', '/budget/apex/BDG_ExpensesSummary', pageUrl == '/budget/apex/BDG_ExpensesSummary'),
			new MenuItem('Budget Details', '/budget/apex/BDG_BudgetDetails', pageUrl == '/budget/apex/BDG_BudgetDetails'),
			new MenuItem('Income', '/budget/apex/BDG_Income', pageUrl == '/budget/apex/BDG_Income'),
			new MenuItem('Shopping List', '/budget/apex/BDG_ShoppingList', pageUrl == '/budget/apex/BDG_ShoppingList'),
            new MenuItem('Veggie Achievement', '/budget/apex/VG_Achievements', pageUrl == '/budget/apex/VG_Achievements'),
            new MenuItem('Admin', '/budget/apex/VG_Admin', pageUrl == '/budget/apex/VG_Admin')    
		};

		return JSON.serialize(config);
	}
}
public with sharing class VG_Admin_Controller {
    public String selectedYear { get; set; }
	public String selectedMonth { get; set; }
    
    public String getMenuConfig() {
        return BDG_Menu_Controller.getItemsForPage('/budget/apex/VG_Admin');
    }
    
    	public List<SelectOption> getYears() {
		Integer currentYear = Date.today().year();

		List<SelectOption> years = new List<SelectOption>();
		years.add(new SelectOption(String.valueOf(currentYear), String.valueOf(currentYear)));
		years.add(new SelectOption(String.valueOf(currentYear + 1), String.valueOf(currentYear + 1)));

		return years;
	}

	public List<SelectOption> getMonths() {
		Integer currentMonth = Date.today().month();
		List<SelectOption> months = new List<SelectOption>();
        for (Integer i = 0; i < 12; i++) {
            Integer month = Math.mod(currentMonth + i, 12);
            month = (month == 0) ? 12 : month;
            String monthLabel = month < 10 ? '0' + String.valueOf(month) : String.valueOf(month);
            months.add(new SelectOption(String.valueOf(month), monthLabel));
        }

		return months;
	}
    
    public void createAchievements() {
        List<Veggie__c> veggies = [Select Id, Name FROM Veggie__c];
        List<Veggie_Achievement__c> achievements = new List<Veggie_Achievement__c> ();
        List<Veggie_Achievement__c> achievementsInDatabase = [SELECT Veggie__c, Veggie__r.Name
                                                              FROM Veggie_Achievement__c 
                                                              WHERE CALENDAR_YEAR(Period__c) = :Integer.valueOf(selectedYear) 
                                                              	AND CALENDAR_MONTH(Period__c) = :Integer.valueOf(selectedMonth)];
        Set<String> existingVeggies = getExistingVeggiesSet(achievementsInDatabase);
       
        for (Veggie__c veg : veggies) {
            if (!existingVeggies.contains(veg.Id)) {
                Veggie_Achievement__c achievementToAdd = new Veggie_Achievement__c ();
            
                achievementToAdd.Ewa__c = 0;
                achievementToAdd.Jedrzej__c = 0;
                achievementToAdd.Marta__c = 0;
                achievementToAdd.Wacek__c = 0;
                achievementToAdd.Period__c = Date.newInstance(Integer.valueOf(selectedYear), Integer.valueOf(selectedMonth), 1);
                achievementToAdd.Veggie__c = veg.Id;
                
                achievements.add(achievementToAdd);
            }
        }
        
        System.debug('spr achievements ' + achievements);
        System.debug('spr achievements ' + JSON.serialize(achievements));
        System.debug('spr achievements in DB ' + JSON.serialize(achievementsInDatabase));
        try {
            insert achievements;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, achievements.size() + ' new VG Achievement(s) created');
            ApexPages.addMessage(myMsg);
        }
        catch (Exception ex) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage());
            ApexPages.addMessage(myMsg);
        }
    }
    
    private Set<String> getExistingVeggiesSet(List<Veggie_Achievement__c> achievementsList) {
        Set<String> veggiesSet = new Set<String>();
        
        for (Veggie_Achievement__c veggieAchievement : achievementsList) {
            veggiesSet.add(veggieAchievement.Veggie__c);
        }
        return veggiesSet;
    }
}
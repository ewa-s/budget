public with sharing class BDG_Admin_Controller {

	public String selectedYear { get; set; }
	public String selectedMonth { get; set; }
	public List<BudgetSetting> generatedCategoryItems { get; set; }
	public List<BudgetSetting> generatedIncomeValues { get; set; }

	public BDG_Admin_Controller() {
		
	}

	public List<SelectOption> getYears() {
		Integer currentYear = Date.today().year();

		List<SelectOption> years = new List<SelectOption>();
		years.add(new SelectOption(String.valueOf(currentYear), String.valueOf(currentYear)));
		years.add(new SelectOption(String.valueOf(currentYear + 1), String.valueOf(currentYear + 1)));

		return years;
	}

	public List<SelectOption> getMonths() {
		Integer currentMonth = Date.today().month();
		List<SelectOption> months = new List<SelectOption>();
		for (Integer i = currentMonth; i <= 12; i++) {
			String monthLabel = i < 10 ? '0' + String.valueOf(i) : String.valueOf(i);
			months.add(new SelectOption(String.valueOf(i), monthLabel));
		}

		return months;
	}



	public PageReference generateValues() {
		generatedCategoryItems = null;
		generatedIncomeValues = null;

		if (budgetAlreadyExists()) {
			addErrorMessage('Budget for this period already exists');
		}
		else {
			generateExpensesValueList();
			generateIncomeValueList();
		}
		
		return null;
	}

	private Boolean budgetAlreadyExists() {
		Integer numOfBudgets = [
				SELECT Count() FROM BDG_Monthly_Budget__c 
				WHERE CALENDAR_YEAR(Date__c) = :Integer.valueOf(selectedYear) 
					AND CALENDAR_MONTH(Date__c) = :Integer.valueOf(selectedMonth)
			];
		return numOfBudgets > 0;
	}

	private void generateExpensesValueList() {
		List<Budget_Default_Value__mdt> defaultValues = [SELECT MasterLabel, Value__c, Order__c FROM Budget_Default_Value__mdt WHERE Type__c = 'Expense Category'];
		generatedCategoryItems = new List<BudgetSetting>();
		for (Budget_Default_Value__mdt bdv : defaultValues) {
			generatedCategoryItems.add(new BudgetSetting(bdv.MasterLabel, bdv.Value__c, bdv.Order__c));
		}
		generatedCategoryItems.sort();
	}

	private void generateIncomeValueList() {
		List<Budget_Default_Value__mdt> defaultValues = [SELECT MasterLabel, Value__c, Order__c FROM Budget_Default_Value__mdt WHERE Type__c = 'Income'];
		generatedIncomeValues = new List<BudgetSetting>();
		for (Budget_Default_Value__mdt bdv : defaultValues) {
			generatedIncomeValues.add(new BudgetSetting(bdv.MasterLabel, bdv.Value__c, bdv.Order__c));
		}
		generatedIncomeValues.sort();
	}


	public PageReference createNewBudget() {
		String newBudgetName = selectedYear + '.' + (Integer.valueOf(selectedMonth) < 10 ? '0' + selectedMonth : selectedMonth);
		Date newBudgetDate = Date.newInstance(Integer.valueOf(selectedYear), Integer.valueOf(selectedMonth), 1);

		BDG_Monthly_Budget__c newBudget = new BDG_Monthly_Budget__c(Name = newBudgetName, Date__c = newBudgetDate);
		insert newBudget;
		createNewIncomeItems(newBudget.Id);
		createNewCategoryItems(newBudget.Id);

		return null;
	}

	private void createNewIncomeItems(Id parentBudgetId) {
		List<BDG_Income__c> newIncomeItems = new List<BDG_Income__c>();

		for (BudgetSetting income : generatedIncomeValues) {
			newIncomeItems.add(
				new BDG_Income__c(
					Month_Budget__c = parentBudgetId, 
					Name = income.name, 
					Amount__c = income.value
				)
			);
		}

		insert newIncomeItems;
	}

	private void createNewCategoryItems(Id parentBudgetId) {
		List<BDG_Category_Balance__c> newCategoryItems = new List<BDG_Category_Balance__c>();

		for (BudgetSetting category : generatedCategoryItems) {
			newCategoryItems.add(
				new BDG_Category_Balance__c(
					Month_Budget__c = parentBudgetId,
					Category__c = category.name,
					Budgeted_Amount__c = category.value,
					Order__c = category.order
				)
			);
		}

		insert newCategoryItems;
	}


	private void addErrorMessage(String message) {
		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, message);
		ApexPages.addMessage(myMsg);
	}

	public class BudgetSetting implements Comparable {
		public String name { get; set; }
		public Decimal value { get; set; }
		public Decimal order { get; set; }

		public BudgetSetting(String name, Decimal value, Decimal order) {
			this.name = name;
			this.value = value;
			this.order = order;
		}

		public Integer compareTo(Object compareTo) {
			BudgetSetting comparingTo = (BudgetSetting)compareTo;
			if (this.order == comparingTo.order) return 0;
			if (this.order > comparingTo.order) return 1;
			return -1;
		}
	}
}
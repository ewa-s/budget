public with sharing class BDG_BudgetDetails_Controller {

	public String selectedBudgetId { get; set; }

	public BDG_BudgetDetails_Controller() {
		selectedBudgetId = getCurrentBudgetId();
	}

	private String getCurrentBudgetId() {
		String result;
		Date now = Date.today();
		BDG_Monthly_Budget__c currentBudget = [SELECT Id FROM BDG_Monthly_Budget__c 
												WHERE CALENDAR_YEAR(Date__c) = :now.year()
													AND CALENDAR_MONTH(Date__c) = :now.month()];
		if (currentBudget != NULL) {
			result = currentBudget.Id;
		}
		return result;
	}

	public List<SelectOption> getBudgets() {
		List<SelectOption> result = new List<SelectOption>();

		for (BDG_Monthly_Budget__c budget : [SELECT Id, Name FROM BDG_Monthly_Budget__c ORDER BY Name]) {
			result.add(new SelectOption(budget.Id, budget.Name));
		}

		return result;
	}

	public BDG_Monthly_Budget__c getBudgetDetails() {
		return [SELECT Id, Name, Already_Spent__c, Date__c, Month_progress__c, Total_Budgeted__c, Total_Income__c,
						Total_Spend__c, Unbudgeted__c
				FROM BDG_Monthly_Budget__c
				WHERE Id = :selectedBudgetId];
	}
	
    public String getMenuConfig() {
        return BDG_Menu_Controller.getItemsForPage('/budget/apex/BDG_BudgetDetails');
    }
}
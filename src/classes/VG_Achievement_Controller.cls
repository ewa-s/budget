public with sharing class VG_Achievement_Controller {
    public String achievementId { get; set; }
    public String achievementColumn { get; set; }
    public List<Veggie_Achievement__c> achievements { get; set; }
    public String selectedDate { get; set; }
    public List<SelectOption> listOfDates { get; set; }
    
    public VG_Achievement_Controller() {
        listOfDates = fetchListOfDates();
        selectedDate = listOfDates[0].getValue();
        achievements = fetchSelectedMonthVeggieAchievements();
    }
     
    private List<Veggie_Achievement__c> fetchSelectedMonthVeggieAchievements() {
        Integer selectedYear = Integer.valueOf(selectedDate.split('\\.')[0]);
        Integer selectedMonth = Integer.valueOf(selectedDate.split('\\.')[1]);
        
        return [SELECT Id, Period__c, Veggie__c, Veggie__r.Name, Veggie__r.Picture_URL__c, Jedrzej__c, Marta__c, Ewa__c, Wacek__c
               	FROM Veggie_Achievement__c
               	WHERE CALENDAR_YEAR(Period__c) = :selectedYear AND CALENDAR_MONTH(Period__c) = :selectedMonth 
               	ORDER BY Veggie__r.Name ASC
               ];
    }
    
    private List<SelectOption> fetchListOfDates() {
    	List<AggregateResult> listOfPeriods = [SELECT Period__c
               FROM Veggie_Achievement__c
               GROUP BY Period__c
               ORDER BY Period__c DESC                             
               ];
        List<SelectOption> result = new List<SelectOption> ();
        
        for (AggregateResult ar : listOfPeriods) {
            String dateYear = String.valueOf(((Date)ar.get('Period__c')).year());
            String dateMonth = String.valueOf(((Date)ar.get('Period__c')).month());
            dateMonth = dateMonth.length() == 1 ? '0' + dateMonth : dateMonth;
            result.add(new SelectOption(dateYear + '.' + dateMonth, dateYear + '.' + dateMonth));
        }
        
        return result;
    }
    
    public Map<String, Integer> getMonthSummary() {
        Map<String, Integer> totalByPerson = new Map<String,Integer> {
        	'Jedrzej__c' => 0,
            'Marta__c' => 0,
            'Ewa__c' => 0,
            'Wacek__c' => 0
        };
        for (Veggie_Achievement__c va : achievements) {
            totalByPerson.put('Jedrzej__c',Integer.valueOf(totalByPerson.get('Jedrzej__c')+va.Jedrzej__c));
            totalByPerson.put('Marta__c',Integer.valueOf(totalByPerson.get('Marta__c')+va.Marta__c));
            totalByPerson.put('Ewa__c',Integer.valueOf(totalByPerson.get('Ewa__c')+va.Ewa__c));
            totalByPerson.put('Wacek__c',Integer.valueOf(totalByPerson.get('Wacek__c')+va.Wacek__c));
        }
        
        return totalByPerson;
    }   
    
    public PageReference veggieEaten() {
        for (Veggie_Achievement__c va : achievements) {
            if (va.Id == achievementId) {
                Integer noVeggiesEaten = Integer.valueOf(va.get(achievementColumn));
                if (noVeggiesEaten == 0) {
                    va.put(achievementColumn, 5);
                } 
                else {
                	va.put(achievementColumn, Integer.valueOf(va.get(achievementColumn)) + 1);
                }
            }
        }
        
        update achievements;
        return null;
    }
    
    public String getMenuConfig() {
        return BDG_Menu_Controller.getItemsForPage('/budget/apex/VG_Achievements');
    }
}
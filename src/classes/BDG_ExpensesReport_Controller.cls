public with sharing class BDG_ExpensesReport_Controller {

	public String selectedBudgetId { get; set; }

	public BDG_ExpensesReport_Controller() {
		selectedBudgetId = getCurrentBudgetId();
	}

	public List<SelectOption> getBudgets() {
		List<SelectOption> result = new List<SelectOption>();

		for (BDG_Monthly_Budget__c budget : [SELECT Id, Name FROM BDG_Monthly_Budget__c ORDER BY Name]) {
			result.add(new SelectOption(budget.Id, budget.Name));
		}

		return result;
	}

	private String getCurrentBudgetId() {
		String result;
		Date now = Date.today();
		BDG_Monthly_Budget__c currentBudget = [SELECT Id FROM BDG_Monthly_Budget__c 
												WHERE CALENDAR_YEAR(Date__c) = :now.year()
													AND CALENDAR_MONTH(Date__c) = :now.month()];
		if (currentBudget != NULL) {
			result = currentBudget.Id;
		}
		return result;
	}

	public List<BDG_Expense__c> getExpensesForBudget() {
		return [SELECT Id, Name, Date__c, Category_Name__c, Amount__c, Category__c, Description__c
				FROM BDG_Expense__c
				WHERE Category__r.Month_Budget__c = :selectedBudgetId
				ORDER BY Date__c ASC];
	}

	
	public String getMenuConfig() {
		return BDG_Menu_Controller.getItemsForPage('/budget/apex/BDG_ExpensesReport');
	}
}